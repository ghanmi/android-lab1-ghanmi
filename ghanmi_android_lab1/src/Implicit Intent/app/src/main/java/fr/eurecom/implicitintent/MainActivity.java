package fr.eurecom.implicitintent;
// By Firas Ghanmi
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.CalendarContract;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.os.Environment.getExternalStoragePublicDirectory;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }




    public void callIntent(View view) {
        Intent intent = null;
        switch (view.getId()) {
            case R.id.call_browser:
                EditText textBrow = (EditText) findViewById(R.id.browserEdit);
                String url = textBrow.getText().toString().trim();
                Uri uri = Uri.parse("http://" + url);
                intent = new Intent(Intent.ACTION_VIEW, uri);
                // Handler
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                } else {
                    Log.d("Intent", "Can't Handle this action");
                }
                break;
            case R.id.call_someone:
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 1);
                }
                EditText phone = findViewById(R.id.someoneEdit);
                Uri number = Uri.parse("tel:" + phone.getText().toString());
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(number);
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) { //it is already checked before, but Android Studio wants it
                    startActivity(callIntent);
                }
                break;
            case R.id.dial:
                Intent dialIntent = new Intent(Intent.ACTION_DIAL);
                // Handler
                if (dialIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(dialIntent);
                } else {
                    Log.d("Intent", "Can't Handle this action");
                }
                break;
            case R.id.show_map:
                Uri emptyLocation = Uri.parse("geo:0,0");
                Intent ShowMapIntent = new Intent(Intent.ACTION_VIEW, emptyLocation);
                // Handler
                if (ShowMapIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(ShowMapIntent);
                } else {
                    Log.d("Intent", "Can't Handle this action");
                }
                break;
            case R.id.search_on_map:
                EditText locationEditText = findViewById(R.id.mapEdit);
                String location = locationEditText.getText().toString();
                Uri adressUri = Uri.parse("geo:0,0?q=" + location);
                Intent searchOnMapIntent = new Intent(Intent.ACTION_VIEW, adressUri);
                // Handler
                if (searchOnMapIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(searchOnMapIntent);
                } else {
                    Log.d("Intent", "Can't Handle this action");
                }
                break;
            case R.id.take_pic:
                // Request runtime camera permission.
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 2);
                }
                //dispatchTakePictureIntent();
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) { //it is already checked before, but Android Studio wants it
                    startActivity(takePictureIntent);
                }
                break;
            case R.id.show_contact:
                Intent show_contact_intent = new Intent(Intent.ACTION_VIEW, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(show_contact_intent, 1);
                break;
            case R.id.edit_first_contact:
                Intent edit_contact_intent = new Intent(android.content.Intent.ACTION_EDIT, Uri.parse("content://com.android.contacts/contacts/1"));
                startActivityForResult(edit_contact_intent, 1);
                break;
            case R.id.chooser:
                Intent intent_ = new Intent(Intent.ACTION_VIEW);
                String title = "Custom example of app chooser";
                Intent chooser = Intent.createChooser(intent_, title);
                if (intent_.resolveActivity(getPackageManager()) != null) {
                    startActivity(chooser);
                }
                break;
            case R.id.add_to_calendar:
                // ACTION_INSERT does not work on all phones
                // use  Intent.ACTION_EDIT in this case
                Intent calendar_intent = new Intent(Intent.ACTION_INSERT);
                calendar_intent.setType("vnd.android.cursor.item/event");
                EditText event_title_ = findViewById(R.id.title_event);
                EditText event_location_ = findViewById(R.id.location_event);
                EditText description_title_ = findViewById(R.id.description_event);
                String event_title = event_title_.getText().toString();
                String event_location = event_location_.getText().toString();
                String description_title = description_title_.getText().toString();
                calendar_intent.putExtra(CalendarContract.Events.TITLE, event_title);
                calendar_intent.putExtra(CalendarContract.Events.EVENT_LOCATION, event_location);
                calendar_intent.putExtra(CalendarContract.Events.DESCRIPTION, description_title);
                // make it a full day event
                calendar_intent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, true);

                calendar_intent.setData(CalendarContract.Events.CONTENT_URI);
                startActivity(calendar_intent);
                break;
        }
    }

    // CAMERA
    String currentPhotoPath;

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        //File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File storageDir = getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    static final int REQUEST_TAKE_PHOTO = 1;
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                String pathToFile = photoFile.getAbsolutePath();
                Uri photoURI = FileProvider.getUriForFile(MainActivity.this,
                        "fr.eurecom.implicitintent.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }




}